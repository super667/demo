from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _

# Create your models here.

DEGREE_TYPE = (
    (u'本科', u'本科'),
    (u'硕士', u'硕士'),
    (u'博士', u'博士'),
)

JobTypes = [
    (0, "技术类"),
    (1, "产品类"),
    (2, "运营类"),
    (3, "设计类"),
    (4, "市场营销类"),
]


Cities = [
    (0, "北京"),
    (1, "上海"),
    (2, "深圳"),
    (3, "杭州"),
    (4, "广州"),
]


class Job(models.Model):
    # Translators: 职位实体的翻译
    job_type = models.SmallIntegerField(blank=False, choices=JobTypes, verbose_name=_("职位类别"))
    job_name = models.CharField(blank=False, max_length=250, verbose_name=_("职位名称"))
    job_city = models.SmallIntegerField(blank=False, choices=Cities, verbose_name=_("工作地点"))
    job_responsibility = models.TextField(blank=False, max_length=1024, verbose_name=_("职位职责"))
    job_requirement = models.TextField(blank=False, max_length=1024, verbose_name=_("职位要求"))
    creator = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, verbose_name=_("创建人"))
    created_date = models.DateTimeField(auto_now_add=True, verbose_name=_("创建日期"))
    modified_date = models.DateTimeField(auto_now=True, verbose_name=_("修改日期"))

    class Meta:
        verbose_name = _("职位")
        verbose_name_plural = _("职位列表")

    def __str__(self):
        return self.job_name


class Resume(models.Model):
    # Translators: 简历实体的翻译
    username = models.CharField(verbose_name=u"姓名", max_length=135)
    applicant = models.ForeignKey(User, verbose_name="申请人", null=True, on_delete=models.SET_NULL)
    city = models.CharField(verbose_name=u"城市", max_length=135)
    phone = models.CharField(verbose_name=u"手机号码", max_length=135)
    email = models.EmailField(verbose_name=u"邮箱", max_length=135, blank=True)
    apply_position = models.CharField(verbose_name=u"应聘职位", max_length=135, blank=True)
    born_address = models.CharField(verbose_name=u"生源地", max_length=135, blank=True)
    gender = models.CharField(verbose_name=u"性别", max_length=135, blank=True)
    picture = models.ImageField(upload_to='image/', verbose_name=u"个人照片", blank=True)
    attachment = models.FileField(upload_to='file/', verbose_name=u"简历附件", blank=True)

    # 学校与学历信息
    bachelor_school = models.CharField(verbose_name="本科学校", blank=True, max_length=135)
    master_school = models.CharField(verbose_name="研究生学校", blank=True, max_length=135)
    doctor_school = models.CharField(verbose_name="博士生学校", blank=True, max_length=135)
    major = models.CharField(verbose_name="专业", blank=True, max_length=135)
    degree = models.CharField(verbose_name="学历", choices=DEGREE_TYPE, blank=True, max_length=135)
    created_date = models.DateTimeField(verbose_name="创建日期", default=datetime.now())
    modified_date = models.DateTimeField(verbose_name="修改日期", auto_now=True)

    # 候选人自我介绍，工作经历，项目经历
    candidate_introduction = models.TextField(max_length=1024, blank=True, verbose_name=u'自我介绍')
    work_experience = models.TextField(max_length=1024, blank=True, verbose_name=u'工作经历')
    project_experience = models.TextField(max_length=1024, blank=True, verbose_name=u'项目经历')

    class Meta:
        verbose_name = "简历"
        verbose_name_plural = "简历列表"

    def __str__(self):
        return self.username

