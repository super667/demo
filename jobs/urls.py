from django.urls import path
from django.conf import settings


from jobs import views


urlpatterns = [
    path("joblist/", views.joblist, name='joblist'),
    path("job/<int:job_id>/", views.detail, name='detail'),

    # 管理员创建HR账号的页面
    path('create_hr_user/', views.create_hr_user, name='create_hr_user'),

    # 简历详情
    path('resume/<int:pk>/', views.ResumeDetailView.as_view(), name='resume-detail'),
    path('resume/add/', views.ResumeCreateView.as_view(), name='resume-add'),
]

if settings.DEBUG:
    urlpatterns += [
        path('detail_resume/<int:resume_id>/', views.detail_resume, name='detail_resume')
    ]