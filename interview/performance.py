import time
import logging
import traceback

logger = logging.getLogger(__name__)


def performance_loggger_middleware(get_response):
    def middleware(request):
        start_time = time.time()
        response = get_response(request)
        duration = time.time() - start_time
        response["X-Page-Duration-ms"] = int(duration*1000)
        logger.info("%s %s %s", duration, request.path, request.GET.dict())
        return response

    return middleware


class PerformanceAndExceptionLoggerMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """
        Code to be executed for eatch request before
        the view are called
        :param request:
        """
        start_time = time.time()
        response = self.get_response(request)
        duration = time.time() - start_time
        response["X-Page-Duration-ms"] = int(duration*1000)
        logger.info("duration:%s url:%s parameters:%s", duration, request, request.GET.dict())

        return response

    def process_exception(self, request, exception):
        if exception:
            message = "url:{url} ** msg:{error} ````{tb}````".format(
                url=request,
                error=repr(exception),
                tb=traceback.format_exc()
            )


from django.utils.deprecation import MiddlewareMixin
from django.shortcuts import HttpResponse

class Test(MiddlewareMixin):
    def process_request(self, request):
        print("这是一个中间件-->test")

    def process_response(self, request, response):
        print("这里是Test的HttpResponse")
        return HttpResponse("这是 Test 返回的 HttpResponse")

    def process_view(self, request, view_function, view_args, view_kwargs):
        """

        :param request:
        :param view_function:
        :param view_args:
        :param view_kwargs:
        :return:
        """
        print("这里是Test的process_view函数")
        print(view_function, type(view_function))


class Test2(MiddlewareMixin):
    def process_request(self, request):
        print("这是一个中间件 --> test2")

    def process_response(self, request, response):
        print("这里是Test2的HttpResponse")
        return HttpResponse("这是Test2返回HttpResponse")

    def process_view(self, request, view_func, view_args, view_kwargs):
        print("这个是Test2的process_view函数")
        print(view_func, type(view_func))





