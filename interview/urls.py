from django.urls import path

from interview import views

urlpatterns = [
    path('', views.index),
]